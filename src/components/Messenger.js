import React from "react";
import { Link } from "gatsby";

const Messenger = () => (
  <div className="messenger-container">
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
    window.fbAsyncInit = function() {
      FB.init({
        xfbml            : true,
        version          : 'v3.3'
      });
    };

    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/pl_PL/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your customer chat code -->
    <div className="fb-customerchat"
      attribution=install_email
      page_id="1532350353722944"
      theme_color="#20cef5"
        logged_in_greeting="Cze&#x15b;&#x107;! Czy mog&#x119; Ci jako&#x15b; pom&#xf3;c?"
        logged_out_greeting="Cze&#x15b;&#x107;! Czy mog&#x119; Ci jako&#x15b; pom&#xf3;c?">
    </div>
  </div>
);

export default Messenger;
